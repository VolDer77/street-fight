import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const fighterImage = createFighterImage(fighter);
  const fighterInfo = createFighterInfo(fighter)
  fighterElement.append(fighterImage, fighterInfo);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterInfo(fighter) {
  const { name, attack, defense, health, } = fighter;
  const container = createElement({ tagName: 'div', className: 'fighter-preview___info' });

  const nameDiv = createElement({
    tagName: 'div',
    className: 'fighter-preview___name',
  });
  const attackDiv = createElement({
    tagName: 'div',
    className: 'fighter-preview___attack',
  });
  const defenseDiv = createElement({
    tagName: 'div',
    className: 'fighter-preview___defense',
  });
  const healthDiv = createElement({
    tagName: 'div',
    className: 'fighter-preview___health',
  });

  nameDiv.innerText = `Name: ${name}`;
  attackDiv.innerText = `Attack: ${attack}`;
  defenseDiv.innerText = `Defense: ${defense}`;
  healthDiv.innerText = `Health: ${health}`;

  container.append(nameDiv, attackDiv, defenseDiv, healthDiv);

  return container;
}
