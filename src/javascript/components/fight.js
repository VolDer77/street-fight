import { controls } from '../../constants/controls';
import { showWinnerModal } from './modal/winner'

export async function fight(firstFighter, secondFighter) {
  let firstFighterCurrentHealth = firstFighter.health;
  let secondFighterCurrentHealth = secondFighter.health;

  let firstFighterCriticalAttack = true;
  let secondFighterCriticalAttack = true;

  const keyLog = {};

  document.body.addEventListener('keydown', async function keyPressHandler(e) {
    const firstFighterIndicator = document.getElementById('left-fighter-indicator');
    const secondFighterIndicator = document.getElementById('right-fighter-indicator');
    keyLog[e.code] = true;

    if (e.code === controls.PlayerOneAttack && !keyLog[controls.PlayerOneBlock]) {
      if (!keyLog[controls.PlayerTwoBlock]) {
        const damage = getDamage(firstFighter, secondFighter);
        secondFighterCurrentHealth -= getHealthAfterAttack(secondFighterIndicator, secondFighterCurrentHealth, damage);
        if (secondFighterCurrentHealth <= 0) {
          secondFighterIndicator.style.width = 0;
          document.body.removeEventListener('keydown', keyPressHandler);
          document.body.removeEventListener('keyup', keyUpHandler);
          showWinnerModal(firstFighter)
          return new Promise((resolve) => {
            resolve(firstFighter)
            // resolve the promise with the winner when fight is over
          }).then(winner => winner)
        }
      }
    }

    if (e.code === controls.PlayerTwoAttack && !keyLog[controls.PlayerTwoBlock]) {
      if (!keyLog[controls.PlayerOneBlock]) {
        const damage = getDamage(secondFighter, firstFighter);
        firstFighterCurrentHealth -= getHealthAfterAttack(firstFighterIndicator, firstFighterCurrentHealth, damage);
        if (firstFighterCurrentHealth <= 0) {
          firstFighterIndicator.style.width = 0;
          document.body.removeEventListener('keydown', keyPressHandler);
          document.body.removeEventListener('keyup', keyUpHandler);
          showWinnerModal(secondFighter)
          return new Promise((resolve) => {
            resolve(secondFighter)
          })
        }
      }
    }

    if (firstFighterCriticalAttack && checkCriticalHitCombination(keyLog, controls.PlayerOneCriticalHitCombination)) {
      const damage = getCriticalDamage(firstFighter);
      secondFighterCurrentHealth -= getHealthAfterAttack(secondFighterIndicator, secondFighterCurrentHealth, damage);
      if (secondFighterCurrentHealth <= 0) {
        secondFighterIndicator.style.width = 0;
        document.body.removeEventListener('keydown', keyPressHandler);
        document.body.removeEventListener('keyup', keyUpHandler);
        showWinnerModal(firstFighter)
        return new Promise((resolve) => {
          resolve(firstFighter)
        })
      }
      firstFighterCriticalAttack = false;
      setTimeout(() => {
        firstFighterCriticalAttack = true;
      }, 10000);
    }

    if (secondFighterCriticalAttack && checkCriticalHitCombination(keyLog, controls.PlayerTwoCriticalHitCombination)) {
      const damage = getCriticalDamage(secondFighter);
      firstFighterCurrentHealth -= getHealthAfterAttack(firstFighterIndicator, firstFighterCurrentHealth, damage);
      if (firstFighterCurrentHealth <= 0) {
        firstFighterIndicator.style.width = 0;
        document.body.removeEventListener('keydown', keyPressHandler);
        document.body.removeEventListener('keyup', keyUpHandler);
        showWinnerModal(secondFighter)
        return new Promise((resolve) => {
          resolve(secondFighter);
        })
      }
      secondFighterCriticalAttack = false;
      setTimeout(() => {
        secondFighterCriticalAttack = true;
      }, 10000);
    }
  })


  function keyUpHandler(e) {
    delete keyLog[e.code];
  }

  document.body.addEventListener('keyup', keyUpHandler);
}

function updateHealthIndicatorWidth (indicator) {
  return `${indicator}px`;
}

function getHealthAfterAttack(fighterIndicator, fighterCurrentHealth, damage) {
  const indicatorWidth = fighterIndicator.offsetWidth
  const updatedHealthIndicator = indicatorWidth - updateHealthIndicator(fighterCurrentHealth, indicatorWidth, damage);
  fighterIndicator.style.width = updateHealthIndicatorWidth(updatedHealthIndicator);

  return damage;
}

function updateHealthIndicator(fighterHealth, indicatorWidth, damage) {
  const result = (damage * indicatorWidth) / fighterHealth;
  return result;
}

function getCriticalDamage(attacker) {
  return 2 * attacker.attack;
}

function checkCriticalHitCombination(keyLog, combination) {
  return keyLog[combination[0]] && keyLog[combination[1]] && keyLog[combination[2]]
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  if (blockPower > hitPower) return 0;
  return hitPower - blockPower;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}