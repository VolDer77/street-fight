import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview'

export function showWinnerModal(fighter) {
  const img = createFighterImage(fighter);
  const resultObj = {
    title: `Winner is ${fighter.name}`,
    bodyElement: img
  }
  showModal(resultObj)
}
